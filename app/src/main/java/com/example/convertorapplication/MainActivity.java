package com.example.convertorapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    LinearLayout input;
    EditText dec1;
    EditText dec2;
    EditText dec3;
    LinearLayout output;
    EditText bin;
    EditText oct;
    EditText hex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        dec1 = (EditText) findViewById(R.id.decimal_to_binary);
        dec2 = (EditText) findViewById(R.id.decimal_to_octal);
        dec3 = (EditText) findViewById(R.id.decimal_to_hexadecimal);

        bin = (EditText) findViewById(R.id.binary);
        oct = (EditText) findViewById(R.id.octal);
        hex = (EditText) findViewById(R.id.hexadecimal);


        dec1.addTextChangedListener(new TextListener() {

            @Override
            public void afterTextChanged(Editable s) {


                bin.setText(Integer.toBinaryString(Integer.parseInt(dec1.getText().toString())));
            }
        });


        dec2.addTextChangedListener(new TextListener() {


            @Override
            public void afterTextChanged(Editable s) {

                oct.setText(Integer.toOctalString(Integer.parseInt(dec2.getText().toString())));
            }
        });

        dec3.addTextChangedListener(new TextListener() {

            @Override
            public void afterTextChanged(Editable s) {

                hex.setText(Integer.toHexString(Integer.parseInt(dec3.getText().toString())));
            }
        });

    }
}
