package com.example.convertorapplication;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Надиа on 22.04.17.
 */

public class TextListener implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
